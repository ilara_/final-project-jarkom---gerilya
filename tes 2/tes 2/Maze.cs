﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace tes_2
{
    public class Maze
    {
        protected string[] textualMaze;
        public Tile[,] tiles = new Tile[28, 31];   

        public static int totalScore = 0;

        public Maze()
        {
            textualMaze = System.IO.File.ReadAllLines("waze.txt");

            for (short i = 0; i < 28; i++)        
            {
                for (short j = 0; j < 31; j++)    
                {
                    tiles[i, j] = new Tile();     
                }
            }

            for (short i = 0; i < 28; i++)        
            {
                for (short j = 0; j < 31; j++)    
                {
                    char readChar = textualMaze[j][i];  

                   

                    tiles[i, j].setX((short)(i * 20)); 
                    tiles[i, j].setY((short)(j * 20)); 

                    if (readChar != 'W')
                    { 
                        tiles[i, j].setWall(false);
                    }
                    else
                    {
                        
                        tiles[i, j].setWall(true);
                    }
                    if (readChar == 'D' || readChar == 'd')    
                    {
                        totalScore += 10;
                        
                        tiles[i, j].setImage("img/dotTile.png");
                    }
                   
            
                    if (i > 0 && i < 27)         
                    {
                        tiles[i, j].setTileToLeft(tiles[i - 1, j]);
                        tiles[i, j].setTileToRight(tiles[i + 1, j]);
                    }
                    if (j > 0 && j < 30)         
                    {
                        tiles[i, j].setTileAbove(tiles[i, j - 1]);
                        tiles[i, j].setTileBelow(tiles[i, j + 1]);
                    }
                }
            }
        }

        public Tile getTile(int i, int j)
        {
            return tiles[i, j];
        }

        public Tile findTile(int i, int j)
        {
            return tiles[i / 20, j / 20];
        }

        public Tile[,] getTiles()
        {
            return this.tiles;
        }
    }
}
