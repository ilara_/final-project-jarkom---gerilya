﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tes_2
{
    public class Logic
    {
        public delegate void ChangedEventHandler(object sender, EventArgs e);

        public event ChangedEventHandler gameStateChange;
        protected void OnGameStateChanged(EventArgs e)
        {
            if (gameStateChange != null)
            {
                gameStateChange(this, e);
            }
        }

        protected int yellowScore = 0;
        public int getYellowScore() { return this.yellowScore; }
        protected int greenScore = 0;
        public int getGreenScore() { return this.greenScore; }

        private string remoteIP;
        public string getRemoteIP() { return this.remoteIP; }
        public void setRemoteIP(string newRemoteIP) { this.remoteIP = newRemoteIP; }

        protected byte direction;
        public void setDirection(byte newDirection) { this.direction = newDirection; }
        protected GameWindow gameWindow;

    

        public Logic(string newRemoteIP, GameWindow gameWindow)
        {
            this.setRemoteIP(newRemoteIP);
            this.gameWindow = gameWindow;
        }
        public virtual void recieveDirection(object sender, EventArgs e) { }
        public virtual void startGame() { }
    }
}