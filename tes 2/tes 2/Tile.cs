﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace tes_2
{
    public class Tile
    {
        protected Tile tileAbove;
        public Tile getTileAbove() { return this.tileAbove; }
        public void setTileAbove(Tile newTile) { this.tileAbove = newTile; }

        protected Tile tileBelow;
        public Tile getTileBelow() { return this.tileBelow; }
        public void setTileBelow(Tile newTile) { this.tileBelow = newTile; }

        protected Tile tileToRight;
        public Tile getTileToRight() { return this.tileToRight; }
        public void setTileToRight(Tile newTile) { this.tileToRight = newTile; }

        protected Tile tileToLeft;
        public Tile getTileToLeft() { return this.tileToLeft; }
        public void setTileToLeft(Tile newTile) { this.tileToLeft = newTile; }

        protected short X;
        public short getX() { return this.X; }
        public void setX(short newX) { this.X = newX; }

        protected short Y;
        public short getY() { return this.Y; }
        public void setY(short newY) { this.Y = newY; }

        protected bool wall;
        public bool isWall() { return wall; }
        public void setWall(bool newWall) { this.wall = newWall; }

        protected Image image;
        public Image getImage() { return this.image; }
        public void setImage(string path)
        {
            this.image = Image.FromFile(path);
        }
        public void setImage(Image otherImage)
        {
            this.image = otherImage;
        }

    

        public Tile() { }
    }
}
