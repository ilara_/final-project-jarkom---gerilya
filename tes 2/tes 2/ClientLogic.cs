﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace tes_2
{
    public class ClientLogic : Logic
    {
        private Thread connectionThread;
        public ClientLogic(string newRemoteIP, GameWindow gameWindow) : base(newRemoteIP, gameWindow)
        {

        }

        public override void startGame()
        {
            base.startGame();
            connectionThread = new Thread(tick);
            connectionThread.Start();
        }

        private void tick()
        {
            while (true)
            {
                
                UdpClient UDPclient = new UdpClient(new IPEndPoint(IPAddress.Any, 12345));
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 12345);
                byte[] data = UDPclient.Receive(ref remoteEndPoint);
                UDPclient.Close();

                string remoteIPint = remoteEndPoint.Address.ToString();

                
                if (data[18] == 0)
                {
                    this.gameWindow.yellowTank.setX(BitConverter.ToInt16(data, 0));
                    this.gameWindow.yellowTank.setY(BitConverter.ToInt16(data, 2));
                    this.gameWindow.yellowTank.setDirection(data[4]);
                    Tile yellowTile = this.gameWindow.maze.findTile(BitConverter.ToInt16(data, 0), BitConverter.ToInt16(data, 2));

                    this.gameWindow.greenTank.setX(BitConverter.ToInt16(data, 5));
                    this.gameWindow.greenTank.setY(BitConverter.ToInt16(data, 7));
                    this.gameWindow.greenTank.setDirection(data[9]);

                    Tile greenTile = this.gameWindow.maze.findTile(BitConverter.ToInt16(data, 5), BitConverter.ToInt16(data, 7));

                    this.yellowScore = BitConverter.ToInt32(data, 10);
                    this.greenScore = BitConverter.ToInt32(data, 14);

                    UDPclient = new UdpClient(remoteIPint, 12000);
                    IPEndPoint dummyPoint = new IPEndPoint(IPAddress.Any, 0);
                    UDPclient.Send(new byte[] { this.direction }, 1);
                    UDPclient.Close();

                    this.OnGameStateChanged(EventArgs.Empty);
                }
                else
                {
                    string textToShow = "";
                    if (data[19] == 0)
                    {
                        textToShow = "Yellow Has won the game! You have lost, Green :-(!";
                    }
                    else if (data[19] == 1)
                    {
                        textToShow = "Green Has won the game! You have Won!";
                    }
                    else
                    {
                        textToShow = "OMG It's a draw!";
                    }
                    MessageBox.Show(textToShow);

                    this.gameWindow.Invoke((MethodInvoker)delegate () { this.gameWindow.unhideMainMenu(); this.gameWindow.Close(); });

                    break;
                }
            }
        }
    }
}