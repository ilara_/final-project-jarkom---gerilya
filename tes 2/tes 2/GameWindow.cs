﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace tes_2
{
    public partial class GameWindow : Form
    {
        
        int ammo = 10;
        public Maze maze;
        public Tank yellowTank;
        public Tank greenTank;
        private Logic gameLogic;
        private GameWindow gameWindow;

        private int yellowScore = 0;
        private int greenScore = 0;

        private MainMenu mainMenu;

        public GameWindow(bool isAHostedGame, string remoteIP, MainMenu mainMenu)
        {
            this.mainMenu = mainMenu;

            InitializeComponent();
            initializeGame();

            if (isAHostedGame)
                this.gameLogic = new HostLogic(remoteIP, this);
            else
                this.gameLogic = new ClientLogic(remoteIP, this);

            gameLogic.gameStateChange += refreshUI;
            gameLogic.startGame();
        }

        public void unhideMainMenu()
        {
            mainMenu.Invoke((MethodInvoker)delegate () { this.mainMenu.Show(); });
        }

        private void initializeGame()
        {
            maze = new Maze();
            yellowTank = new Tank(maze.getTile(1, 1), "img/up.png", 3, true,true, greenTank);
            greenTank = new Tank(maze.getTile(25, 25), "img/zup.png", 2, false,false, yellowTank);
        }

        private void refreshUI(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void GameWindow_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                Graphics g = e.Graphics;
                foreach (Tile t in maze.getTiles())
                {

                }
                g.DrawImage((Image)yellowTank.getImage().Clone(), new Point(yellowTank.getX() - 8, yellowTank.getY() - 8));
                g.DrawImage((Image)greenTank.getImage().Clone(), new Point(greenTank.getX() - 8, greenTank.getY() - 8));

                yellowScoreLabel.Text = "" + gameLogic.getYellowScore();
                greenScoreLabel.Text = "" + gameLogic.getGreenScore();
            }
            catch (Exception excp)
            {

            }
        }
      
       
    

        private void GameWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.gameLogic.setDirection(0);

            }
            else if (e.KeyCode == Keys.Right)
            {
                this.gameLogic.setDirection(1);

            }
            else if (e.KeyCode == Keys.Down)
            {
                this.gameLogic.setDirection(2);

            }
            else if (e.KeyCode == Keys.Left)
            {
                this.gameLogic.setDirection(3);

            }
            //??????
            else if (e.KeyCode == Keys.Space )
            {
                this.gameLogic.setDirection(4);
            }
        }

        private void GameWindow_Load(object sender, EventArgs e)
        {

        }

        
        private void GameWindow_keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                this.gameLogic.setDirection(0);

            }
            else if (e.KeyCode == Keys.Right)
            {
                this.gameLogic.setDirection(1);

            }
            else if (e.KeyCode == Keys.Down)
            {
                this.gameLogic.setDirection(2);

            }
            else if (e.KeyCode == Keys.Left)
            {
                this.gameLogic.setDirection(3);

            }
            //??????
            else if (e.KeyCode == Keys.Space && ammo > 0)
            {
                this.gameLogic.setDirection(4);
            }
        }

        
    }
}



