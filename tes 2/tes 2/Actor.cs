﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization;

namespace tes_2
{
    public class Actor
    {
        //posisi x y dari aktor
        protected short X;
        public short getX() { return this.X; }
        public void setX(short newX) { this.X = newX; }

        protected short Y;
        public short getY() { return this.Y; }
        public void setY(short newY) { this.Y = newY; }

        //byte arah
        protected byte direction;
        public byte getDirection() { return this.direction; }

        protected Tile currentTile;
        public Tile getCurrentTile() { return this.currentTile; }
        public void setCurrentTile(Tile newTile)
        {
            this.currentTile = newTile;
            this.X = newTile.getX();
            this.Y = newTile.getY();
        }

        public void move()
        {
            Tile destinationTile = null;
            if (this.direction == 0)
            {
                destinationTile = this.getCurrentTile().getTileAbove();
            }
            else if (this.direction == 1)
            {
                destinationTile = this.getCurrentTile().getTileToRight();
            }
            else if (this.direction == 2)
            {
                destinationTile = this.getCurrentTile().getTileBelow();
            }
            else if (this.direction == 3)
            {
                destinationTile = this.getCurrentTile().getTileToLeft();
            }

            //mengecek apa ada jalan yang bisa dilewati
            if (destinationTile != null && (!destinationTile.isWall()))
            {
                setCurrentTile(destinationTile);
            }
        }

        //parameter ubin
        public Actor(Tile initialTile)
        {
            this.setCurrentTile(initialTile);
        }

        protected Actor()
        {

        }
    }
}
