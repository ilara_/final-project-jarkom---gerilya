﻿namespace tes_2
{
    partial class GameWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yellowScoreLabel = new System.Windows.Forms.Label();
            this.greenScoreLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // yellowScoreLabel
            // 
            this.yellowScoreLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.yellowScoreLabel.BackColor = System.Drawing.Color.Black;
            this.yellowScoreLabel.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yellowScoreLabel.ForeColor = System.Drawing.Color.SteelBlue;
            this.yellowScoreLabel.Location = new System.Drawing.Point(293, 319);
            this.yellowScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.yellowScoreLabel.Name = "yellowScoreLabel";
            this.yellowScoreLabel.Size = new System.Drawing.Size(161, 26);
            this.yellowScoreLabel.TabIndex = 0;
            this.yellowScoreLabel.Text = "0";
            this.yellowScoreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // greenScoreLabel
            // 
            this.greenScoreLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.greenScoreLabel.BackColor = System.Drawing.Color.Black;
            this.greenScoreLabel.Font = new System.Drawing.Font("Courier New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.greenScoreLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.greenScoreLabel.Location = new System.Drawing.Point(293, 369);
            this.greenScoreLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.greenScoreLabel.Name = "greenScoreLabel";
            this.greenScoreLabel.Size = new System.Drawing.Size(161, 26);
            this.greenScoreLabel.TabIndex = 1;
            this.greenScoreLabel.Text = "0";
            this.greenScoreLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GameWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(747, 763);
            this.ControlBox = false;
            this.Controls.Add(this.greenScoreLabel);
            this.Controls.Add(this.yellowScoreLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GameWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tank Multiplayer";
            this.Load += new System.EventHandler(this.GameWindow_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.GameWindow_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GameWindow_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.GameWindow_keyisup);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label greenScoreLabel;
        private System.Windows.Forms.Label yellowScoreLabel;
    }
}