﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace tes_2
{
    public class HostLogic : Logic
    {
        private Thread connectionThread;
        public HostLogic(string newRemoteIP, GameWindow gameWindow) : base(newRemoteIP, gameWindow)
        {

        }

        public override void startGame()
        {
            base.startGame();
            connectionThread = new Thread(tick);
            connectionThread.Start();
        }

        public void tick()
        {
            byte qwe = 0;
            Thread.Sleep(2000);
            while (true)
            {
                Thread.Sleep(280);

                this.gameWindow.yellowTank.setDirection(this.direction);
                this.gameWindow.greenTank.setDirection(qwe);

                if (qwe == 4)
                {
                    this.greenScore += 1;
                }

                if (this.direction == 4)
                {
                    this.yellowScore += 1;
                }

                this.gameWindow.yellowTank.setTarget(this.gameWindow.greenTank);
                this.gameWindow.greenTank.setTarget(this.gameWindow.yellowTank);

                this.gameWindow.yellowTank.move();
                this.gameWindow.greenTank.move();
               

                byte[] data = new byte[40];

                byte[] temp = BitConverter.GetBytes(this.gameWindow.yellowTank.getX());
                data[0] = temp[0];
                data[1] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.yellowTank.getY());
                data[2] = temp[0];
                data[3] = temp[1];

                data[4] = (byte)this.gameWindow.yellowTank.getDirection();

                temp = BitConverter.GetBytes(this.gameWindow.greenTank.getX());
                data[5] = temp[0];
                data[6] = temp[1];

                temp = BitConverter.GetBytes(this.gameWindow.greenTank.getY());
                data[7] = temp[0];
                data[8] = temp[1];

                data[9] = (byte)this.gameWindow.greenTank.getDirection();

                

                temp = BitConverter.GetBytes(yellowScore);
                data[10] = temp[0];
                data[11] = temp[1];
                data[12] = temp[2];
                data[13] = temp[3];

                temp = BitConverter.GetBytes(greenScore);
                data[14] = temp[0];
                data[15] = temp[1];
                data[16] = temp[2];
                data[17] = temp[3];

                data[18] = 0; // 1 if game has fin
                data[19] = 0; // 0=yellow, 1=green, 2=draw :(

                if (this.gameWindow.yellowTank.targetDies(this.gameWindow.yellowTank))
                {
                    if (greenScore > yellowScore)
                    {
                        data[18] = 1;
                        if (this.gameWindow.yellowTank.getTarget().isGreen())
                        {
                            data[19] = 1;
                        }
                    }
                }

                if (this.gameWindow.yellowTank.targetDies(this.gameWindow.yellowTank))
                {
                    if (greenScore < yellowScore)
                    {
                        data[18] = 1;

                        if (this.gameWindow.yellowTank.getTarget().isGreen())
                        {
                            data[19] = 0;
                        }


                    }
                }

                if (this.gameWindow.greenTank.targetDies(this.gameWindow.greenTank))
                {
                    if (yellowScore > greenScore)
                    {
                        data[18] = 1;
                        if (this.gameWindow.greenTank.getTarget().isYellow())
                        {
                            data[19] = 0;
                        }

                    }
                }

                if (this.gameWindow.greenTank.targetDies(this.gameWindow.greenTank))
                {
                    if (yellowScore < greenScore)
                    {
                        data[18] = 1;
                        if (this.gameWindow.greenTank.getTarget().isYellow())
                        {
                            data[19] = 1;
                        }

                    }
                }

              

                if (data[18] == 1)
                {
                    string textToShow = "";
                    if (data[19] == 0)
                    {
                        textToShow = "Yellow Has won the game! You have Won!";
                    }
                    else if (data[19] == 1)
                    {
                        textToShow = "Green Has won the game! You have lost, Yellow :-(";
                    }
                    else
                    {
                        textToShow = "OMG It's a draw!";
                    }
                    Thread t = new Thread(new ParameterizedThreadStart(exit));
                    t.Start(textToShow);
                    this.gameWindow.Invoke((MethodInvoker)delegate () { this.gameWindow.unhideMainMenu(); this.gameWindow.Close(); });
                }

                bool recieved = false;
                while (!recieved)
                {
                    try
                    {
                        UdpClient UDPclient = new UdpClient(getRemoteIP(), 12345);
                        IPEndPoint dummyPoint = new IPEndPoint(IPAddress.Any, 0);
                        UDPclient.Send(data, data.Length);
                        
                        IPEndPoint localEP = new IPEndPoint(IPAddress.Any, 12000);
                        UdpClient UDPClient = new UdpClient(localEP);
                        IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);

                        qwe = UDPClient.Receive(ref remoteEP)[0];
                        UDPClient.Close();
                        recieved = true;

                        Console.WriteLine("client direction: " + qwe);
                        Console.WriteLine("server direction: " + this.direction);
                    }
                    catch (Exception e)
                    {
                        recieved = false;
                    }
                }
                if (data[38] == 1) break;
                this.OnGameStateChanged(EventArgs.Empty);
            }
        }
        public void exit(object o)
        {
            MessageBox.Show((string)o);
        }
    }
}
