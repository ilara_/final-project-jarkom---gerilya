﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace tes_2
{
    public partial class MainMenu : Form
    {
        private bool isHost;
        private string remoteIP;
        private static string remoteIP2;

        private Thread connectionThread;
        private static UdpClient client;
        private static UdpClient client2;

        public MainMenu()
        {
            InitializeComponent();
        }

        private void waitForClientResponse()
        {
            client = new UdpClient(new IPEndPoint(IPAddress.Any, 12345));
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 12345);


            byte[] data = client.Receive(ref remoteEndPoint);
            remoteIP = remoteEndPoint.Address.ToString();
            if (ASCIIEncoding.ASCII.GetString(data).Equals("JOIN_REQUEST"))
            {
                client.Send(ASCIIEncoding.ASCII.GetBytes("JOIN_REQUEST_ACCEPTED"), 21, remoteEndPoint);
                isHost = true;
                statusLabel.Invoke((MethodInvoker)delegate () { statusLabel.Text = "A Player has joined the game!"; });
                openGameWindow();
            }
            client.Close();
        }

        private void sendConnectionRequest()
        {
            MainMenu.client2 = new UdpClient(hostIPTextBox.Text, 12345);
            MainMenu.client2.Send(ASCIIEncoding.ASCII.GetBytes("JOIN_REQUEST"), 12);
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse(hostIPTextBox.Text), 12345);

            byte[] data = MainMenu.client2.Receive(ref remoteEndPoint);
            MainMenu.remoteIP2 = remoteEndPoint.Address.ToString();
            if (ASCIIEncoding.ASCII.GetString(data).Equals("JOIN_REQUEST_ACCEPTED"))
            {
                isHost = false;
                statusLabel.Invoke((MethodInvoker)delegate () { statusLabel.Text = "You have joined the game!"; });
                openGameWindow();
            }
            MainMenu.client2.Close();
        }

        private void openGameWindow()
        {
            Thread t = new Thread(ThreadProc);
            t.Start();
        }

        private void ThreadProc()
        {
            this.Invoke((MethodInvoker)delegate () { this.Hide(); });
            Application.Run(new GameWindow(isHost, remoteIP, this));
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (connectionThread != null)
            {
                if (client != null)
                {
                    client.Close();
                }
                connectionThread.Abort();
            }
        }

 

        private void hostGameButton_Click(object sender, EventArgs e)
        {
            hostGameButton.Enabled = false;
            joinGameButton.Enabled = false;
            hostIPTextBox.Enabled = false;
            statusLabel.Text = "Waiting for a player to join...";
            try
            {
                connectionThread = new Thread(waitForClientResponse);
                connectionThread.Start();
            }
            catch (Exception exception)
            {
                hostGameButton.Enabled = true;
                joinGameButton.Enabled = true;
                hostIPTextBox.Enabled = true;
                statusLabel.Text = "Error awaiting a player to join! Please try again.";
            }
        }

        private void joinGameButton_Click(object sender, EventArgs e)
        {
            hostGameButton.Enabled = false;
            joinGameButton.Enabled = false;
            hostIPTextBox.Enabled = false;
            statusLabel.Text = "Joining game...";
            try
            {
                connectionThread = new Thread(sendConnectionRequest);
                connectionThread.Start();
            }
            catch (Exception exception)
            {
                hostGameButton.Enabled = true;
                joinGameButton.Enabled = true;
                hostIPTextBox.Enabled = true;
                statusLabel.Text = "Error sending join request! Please try again.";
            }
        }
    }
}