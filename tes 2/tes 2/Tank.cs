﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.Serialization;

namespace tes_2
{
    public class Tank : Actor
    {
        public Tank(Tile initialTile, string imagePath, byte initialDirection, bool isYellow, bool isGreen, Tank target)
            : base(initialTile)
        {
            this.setImage(imagePath);
            this.setDirection(initialDirection);
            this.yellow = isYellow;
            this.green = isGreen;
            this.target = target;
        }

        protected Tank target;
        public Tank getTarget() { return this.target; }
        public void setTarget(Tank newTarget)
        {
            this.target = newTarget;
        }

        private bool yellow;
        public bool isYellow() { return this.yellow; }

        private bool green;
        public bool isGreen() { return this.green; }

        protected Image image;
        public Image getImage() { return this.image; }
        public void setImage(string imagePath)
        {
            this.image = Image.FromFile(imagePath);
            this.copyImage = (Image)this.image.Clone();
        }
        public void setImage(Image otherImage)
        {
            this.image = (Image)otherImage.Clone();
        }

        protected Image copyImage;
        

        public bool targetDies(Tank iki)
        {
            int xDifference = this.getX() - target.getX();
            int yDifference = this.getY() - target.getY();
            return (xDifference == 0 && yDifference == 0);
        }

        public void setDirection(byte newDirection)
        {
            this.direction = newDirection;

            this.image = (Image)this.copyImage.Clone();
            if (this.direction == 1)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipNone);
            }
            else if (this.direction == 0)
            {
                this.image.RotateFlip(RotateFlipType.Rotate270FlipNone);
            }
            else if (this.direction == 2)
            {
                this.image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            }
            else if (this.direction == 3)
            {
                this.image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            }
            else if (this.direction == 4)
            {
                //this.image.RotateFlip(RotateFlipType.Rotate270FlipNone);

            }
        }

        protected Tank() : base()
        {

        }

        public Tank clone()
        {
            Tank copy = new Tank();
            copy.setCurrentTile(this.getCurrentTile());
            copy.setDirection(this.getDirection());
            copy.setImage(this.getImage());
            copy.setX(this.getX());
            copy.setY(this.getY());
            return copy;
        }
    }
}
